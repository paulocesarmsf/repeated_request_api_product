# -*- coding: utf-8 -*-
import unittest
from workers.comparator import Comparator


class TestComparator(unittest.TestCase):

    def setUp(self):
        self.input_data = 'trãnsform för compare\ntrãnsform för compare'

    def test_transform_for_compare(self):
        expected_data = 'transformforcomparetransformforcompare'
        self.assertEquals(Comparator.transform_for_compare(self.input_data), expected_data)

    def test_create_hash(self):
        input_hash = Comparator.create_hash(self.input_data)
        expected_hash = Comparator.create_hash('transformforcomparetransformforcompare')
        self.assertEquals(input_hash, expected_hash)
