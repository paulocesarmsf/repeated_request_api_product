# -*- coding: utf-8 -*-
import unittest
from mongoengine import connect

from models.request_api_product import RequestApiProduct
from workers.request_api_product_manager import RequestApiProductManager


class TestRequestApiProductManager(unittest.TestCase):

    def setUp(self):
        connect('mongoenginetest', host='mongomock://localhost')

    def test_is_repeated_request_false(self):
        request_api_manager = RequestApiProductManager()
        is_repeated = request_api_manager.is_repeated_request('[{"id": "1234", "name": "cadeira"}]')
        self.assertFalse(is_repeated)

    def test_is_repeated_request_true(self):
        request_api_manager = RequestApiProductManager()
        request_api_manager.is_repeated_request('[{"id": "123", "name": "mesa"}]')
        is_repeated = request_api_manager.is_repeated_request('[{"id": "123", "name": "mesa"}]')
        self.assertTrue(is_repeated)
