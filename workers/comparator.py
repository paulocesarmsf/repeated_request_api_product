import unidecode
import hashlib


class Comparator(object):
    to_ignore = ['\r\n', '\r', '\n']

    @staticmethod
    def transform_for_compare(content):
        content = unidecode.unidecode(content)

        for ignore_str in Comparator.to_ignore:
            content = content.replace(ignore_str, '')
        return content.replace(' ', '')

    @staticmethod
    def create_hash(content):
        transform_content = Comparator.transform_for_compare(content)
        content_hash = hashlib.md5()
        content_hash.update(transform_content.encode('utf-8'))
        return content_hash.hexdigest()

