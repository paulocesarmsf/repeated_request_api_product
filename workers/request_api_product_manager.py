from models.request_api_product import RequestApiProduct
from workers.comparator import Comparator


class RequestApiProductManager(object):

    def is_repeated_request(self, incoming_data):
        hash_request = Comparator.create_hash(incoming_data)
        request_api_product = RequestApiProduct.objects(hash_request=hash_request).first()
        if not request_api_product:
            self.create_request_api_product(hash_request=hash_request)
            return False
        return True

    def create_request_api_product(self, hash_request):
        request_api_product = RequestApiProduct(hash_request=hash_request)
        request_api_product.save()
        return request_api_product
