## Prerequisite

- **Docker** - https://docs.docker.com/install/linux/docker-ce/ubuntu/
- **Docker Compose** - https://docs.docker.com/compose/install/

## Steps for run

1. git clone git@bitbucket.org:paulocesarmsf/repeated_request_api_product.git
2. docker-compose down && docker-compose up (at root the project)


## Steps for run local

1. git clone git@bitbucket.org:paulocesarmsf/repeated_request_api_product.git
2. pip install -r requirements.txt (at root the project)
3. docker run --name mongodb -p 27017:27017 -d mongo


## Run tests

1. nose2 -v (at root the project)
