from flask import Flask, request, jsonify
from models.mongo_database import configure_mongo
from workers.request_api_product_manager import RequestApiProductManager

app = Flask(__name__)

request_api_manager = RequestApiProductManager()


@app.route('/spec', methods=['GET'])
def index():
    return 'ok'


@app.route('/create_product', methods=['POST'])
def create_product():
    if not request_api_manager.is_repeated_request(str(request.data)):
        # create product
        return jsonify(success=True)
    return jsonify(success=False, data={'message': 'repeated request'}), 400


if __name__ == '__main__':
    configure_mongo()
    app.run(host='0.0.0.0', port=5000)
