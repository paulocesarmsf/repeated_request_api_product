from mongoengine import Document, DateTimeField, StringField
from datetime import datetime


class RequestApiProduct(Document):
    meta = {'collection': 'requests_api_product',
            'indexes': [
                {'fields': ['created'], 'expireAfterSeconds': 60*10}
            ]
    }

    created = DateTimeField(default=datetime.utcnow)
    hash_request = StringField(max_length=250, required=True)
